﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework_Theme_04
{
    class Program
    {
        
        static void Main(string[] args)
        {
            #region Задание №1
            Random random = new Random();

            int[,] array = new int[12, 4];

            int[] profitNegative = new int[12];

            int profitPositive = 0;

            string months = "";

            Console.WriteLine($"{"Номер",5} {"Доход, руб.",15} {"Расход, руб.",15} {"Прибыль, руб.",15}");
            for (int i = 0; i < 12; i++)
            {
                array[i, 0] = i + 1;
                array[i, 1] = random.Next(5, 16) * 10000;
                array[i, 2] = random.Next(5, 16) * 10000;
                array[i, 3] = array[i, 1] - array[i, 2];
                profitNegative[i] = array[i, 3];
                Console.WriteLine($"{array[i, 0],5} {array[i, 1],15} {array[i, 2],15} {array[i, 3],15}");
            }

            profitNegative = profitNegative.Distinct().ToArray();
            Array.Sort(profitNegative);

            for (int j = 0; j < 3; j++)
            {
                for (int i = 0; i < 12; i++)
                {
                    if (array[i, 3] > 0 && j == 0)
                    {
                        profitPositive++;
                    }
                    if (array[i, 3] == profitNegative[j])
                    {
                        if (String.IsNullOrWhiteSpace(months))
                        {
                            months = (i + 1).ToString();
                        }
                        else
                        {
                            months = months + ", " + (i + 1).ToString();
                        }
                    }
                }
            }

            Console.WriteLine("");

            Console.WriteLine($"Худшая прибыль в месяцах: {months}");
            Console.WriteLine($"Месяцев с положительной прибылью: {profitPositive}");
            Console.ReadKey();
            #endregion

            #region Задание №3.1

            Console.Clear();

            Random rand = new Random();
            //первыя матрица
            Console.Write("Введите количество столбцов первой матрицы: \t");
            int columm_A = int.Parse(Console.ReadLine());
            Console.Write("Введите количество строк первой матрицы: \t");
            int row_A = int.Parse(Console.ReadLine());
                        
            int [,] matrix_A = new int[columm_A, row_A];
            for (int i=0; i < matrix_A.GetLength(0); i++)
            {
                for (int j = 0; j < matrix_A.GetLength(1); j++)
                {
                    matrix_A[i, j] = rand.Next(10);
                    Console.Write($"{matrix_A[i,j]} ");
                }
                Console.WriteLine();
            }

            //вторая матрица
            Console.Write("Введите столбцов второй матрицы: \t");
            int columm_B = int.Parse(Console.ReadLine());
            Console.Write("Введите количество строк второй матрицы: \t");
            int row_B = int.Parse(Console.ReadLine());
            
            int[,] matrix_B = new int[columm_B, row_B];

            for (int k = 0; k < matrix_B.GetLength(0); k++)
            {
                for (int l = 0; l < matrix_B.GetLength(1); l++)
                {
                    matrix_B[k, l] = rand.Next(10);
                    Console.Write($"{matrix_B[k,l]} ");
                }
                Console.WriteLine();
            }

            int[,] Multiplication(int[,] a, int[,] b)
            {
                int[,] result = new int[a.GetLength(0), b.GetLength(1)];
                for (int i = 0; i < a.GetLength(0); i++)
                {
                    for (int j = 0; j < b.GetLength(1); j++)
                    {
                        for (int k = 0; k < b.GetLength(0); k++)
                        {
                            result [i, j] += a[i, k] * b[k, j];
                            Console.Write("{0} ", a[i, j]);
                        }
                        Console.WriteLine();
                    }
                }
                return result;
            }
            Console.WriteLine("\n Результат умноения матрицы 1 на матрицу 2:");
            int[,] MultiMatrix = Multiplication(matrix_A, matrix_B);
            
            Console.ReadKey();
            #endregion
        }

        #region Описание домашнего задания
        // Задание 1.
        // Заказчик просит вас написать приложение по учёту финансов
        // и продемонстрировать его работу.
        // Суть задачи в следующем: 
        // Руководство фирмы по 12 месяцам ведет учет расходов и поступлений средств. 
        // За год получены два массива – расходов и поступлений.
        // Определить прибыли по месяцам
        // Количество месяцев с положительной прибылью.
        // Добавить возможность вывода трех худших показателей по месяцам, с худшей прибылью, 
        // если есть несколько месяцев, в некоторых худшая прибыль совпала - вывести их все.
        // Организовать дружелюбный интерфейс взаимодействия и вывода данных на экран

        // Пример
        //       
        // Месяц      Доход, тыс. руб.  Расход, тыс. руб.     Прибыль, тыс. руб.
        //     1              100 000             80 000                 20 000
        //     2              120 000             90 000                 30 000
        //     3               80 000             70 000                 10 000
        //     4               70 000             70 000                      0
        //     5              100 000             80 000                 20 000
        //     6              200 000            120 000                 80 000
        //     7              130 000            140 000                -10 000
        //     8              150 000             65 000                 85 000
        //     9              190 000             90 000                100 000
        //    10              110 000             70 000                 40 000
        //    11              150 000            120 000                 30 000
        //    12              100 000             80 000                 20 000
        // 
        // Худшая прибыль в месяцах: 7, 4, 1, 5, 12
        // Месяцев с положительной прибылью: 10


        // * Задание 2
        // Заказчику требуется приложение строящее первых N строк треугольника паскаля. N < 25
        // 
        // При N = 9. Треугольник выглядит следующим образом:
        //                                 1
        //                             1       1
        //                         1       2       1
        //                     1       3       3       1
        //                 1       4       6       4       1
        //             1       5      10      10       5       1
        //         1       6      15      20      15       6       1
        //     1       7      21      35      35       21      7       1
        //                                                              
        //                                                              
        // Простое решение:                                                             
        // 1
        // 1       1
        // 1       2       1
        // 1       3       3       1
        // 1       4       6       4       1
        // 1       5      10      10       5       1
        // 1       6      15      20      15       6       1
        // 1       7      21      35      35       21      7       1
        // 
        // Справка: https://ru.wikipedia.org/wiki/Треугольник_Паскаля


        // 
        // * Задание 3.1
        // Заказчику требуется приложение позволяющщее умножать математическую матрицу на число
        // Справка https://ru.wikipedia.org/wiki/Матрица_(математика)
        // Справка https://ru.wikipedia.org/wiki/Матрица_(математика)#Умножение_матрицы_на_число
        // Добавить возможность ввода количество строк и столцов матрицы и число,
        // на которое будет производиться умножение.
        // Матрицы заполняются автоматически. 
        // Если по введённым пользователем данным действие произвести невозможно - сообщить об этом
        //
        // Пример
        //
        //      |  1  3  5  |   |  5  15  25  |
        //  5 х |  4  5  7  | = | 20  25  35  |
        //      |  5  3  1  |   | 25  15   5  |
        //
        //
        // ** Задание 3.2
        // Заказчику требуется приложение позволяющщее складывать и вычитать математические матрицы
        // Справка https://ru.wikipedia.org/wiki/Матрица_(математика)
        // Справка https://ru.wikipedia.org/wiki/Матрица_(математика)#Сложение_матриц
        // Добавить возможность ввода количество строк и столцов матрицы.
        // Матрицы заполняются автоматически
        // Если по введённым пользователем данным действие произвести невозможно - сообщить об этом
        //
        // Пример
        //  |  1  3  5  |   |  1  3  4  |   |  2   6   9  |
        //  |  4  5  7  | + |  2  5  6  | = |  6  10  13  |
        //  |  5  3  1  |   |  3  6  7  |   |  8   9   8  |
        //  
        //  
        //  |  1  3  5  |   |  1  3  4  |   |  0   0   1  |
        //  |  4  5  7  | - |  2  5  6  | = |  2   0   1  |
        //  |  5  3  1  |   |  3  6  7  |   |  2  -3  -6  |
        //
        // *** Задание 3.3
        // Заказчику требуется приложение позволяющщее перемножать математические матрицы
        // Справка https://ru.wikipedia.org/wiki/Матрица_(математика)
        // Справка https://ru.wikipedia.org/wiki/Матрица_(математика)#Умножение_матриц
        // Добавить возможность ввода количество строк и столцов матрицы.
        // Матрицы заполняются автоматически
        // Если по введённым пользователем данным действие произвести нельзя - сообщить об этом
        //  
        //  |  1  3  5  |   |  1  3  4  |   | 22  48  57  |
        //  |  4  5  7  | х |  2  5  6  | = | 35  79  95  |
        //  |  5  3  1  |   |  3  6  7  |   | 14  36  45  |
        //
        //  
        //                  | 4 |   
        //  |  1  2  3  | х | 5 | = | 32 |
        //                  | 6 |  
        //
        #endregion
    }
}
